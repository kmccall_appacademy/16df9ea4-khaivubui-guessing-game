# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.
def guessing_game
  correct_answer = rand(1..100)
  guess = 0
  number_of_guesses = 0
  until guess == correct_answer
    puts 'Please guess a number'
    guess = gets.to_i
    puts "Your guess is #{guess}"
    puts 'Your guess is too high' if guess > correct_answer
    puts 'Your guess is too low' if guess < correct_answer
    number_of_guesses += 1
  end
  puts "CONGRATULATIONS! YOU GUESSED #{correct_answer} CORRECTLY IN JUST
       #{number_of_guesses} guesses"
end

def file_shuffler
  puts 'Please enter the location of the file you would like to shuffle'
  file_name = gets.chomp
  array_of_lines = File.readlines(file_name)
  array_of_lines.shuffle!
  p array_of_lines
  File.open('_shuffled.txt', 'w') do |f|
    array_of_lines.each do |line|
      f.puts line
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  file_shuffler
end
